package com.kms.ustek_msg.misc.dialogs

import android.content.Context
import android.util.AttributeSet
import com.kms.ustek_msg.R
import com.kms.ustek_msg.base.ABaseView
import com.kms.ustek_msg.domain.repositories.models.DialogItem
import com.kms.ustek_msg.domain.repositories.models.TestDialogItem
import com.kms.ustek_msg.domain.repositories.models.UsersItem
import com.kms.ustek_msg.domain.repositories.models.rest.Message
import com.kms.ustek_msg.domain.repositories.models.rest.User
import kotlinx.android.synthetic.main.view_dialog_type_1.view.*

class Type1View @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ABaseView(context, attrs, defStyleAttr), ITypeView {

    override fun getViewId(): Int = R.layout.view_dialog_type_1

    override fun bind(data: TestDialogItem) {
        tvTitle.text = data.title
    }

    override fun bind(data: UsersItem) {
        TODO("Not yet implemented")
    }

    override fun bind(data: DialogItem) {
        data.abonUser?.let { tvTitle.text = it.login }
    }

    override fun bind(data: User) {
        TODO("Not yet implemented")
    }

    override fun bind(data: Message) {
        tvTitle.text = data.message ?: "undefined"
    }
}