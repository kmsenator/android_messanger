package com.kms.ustek_msg.misc.dialogs

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.kms.ustek_msg.R
import com.kms.ustek_msg.base.ABaseAdapter
import com.kms.ustek_msg.base.ABaseListFragment
import com.kms.ustek_msg.domain.di.components.DaggerAppComponent
import com.kms.ustek_msg.domain.repositories.models.TestDialogItem
import javax.inject.Inject

class TestFragment : ABaseListFragment<TestDialogItem, RecyclerView.ViewHolder>(), ITestDialogsView {

    class Adapter : ABaseAdapter<TestDialogItem, RecyclerView.ViewHolder>() {

        companion object {
            const val TYPE_1 = 0
            const val TYPE_2 = 1
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

            val view: View = if (viewType == TYPE_1) Type1View(parent.context)
            else Type2View(parent.context)
            view.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            return object : RecyclerView.ViewHolder(view) { }
        }

        override fun getItemViewType(position: Int): Int {
            return if (data[position].isType1) TYPE_1 else TYPE_2
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            val view = holder.itemView
            if (view is ITypeView)
                view.bind(data[position])
        }
    }

    override fun getListId(): Int = R.id.rvList
    override fun getViewId(): Int = R.layout.fragment_dialogs

    @Inject
    @InjectPresenter
    lateinit var presenter: TestPresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    private val adapter = Adapter()

    override fun provideAdapter() = adapter

    override fun inject() {
        DaggerAppComponent.create().inject(this)
    }

    override fun bindDialogs(testDialogs: List<TestDialogItem>) {
        adapter.data = testDialogs.toMutableList()
    }
}