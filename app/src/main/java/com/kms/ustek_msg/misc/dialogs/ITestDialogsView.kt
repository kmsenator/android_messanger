package com.kms.ustek_msg.misc.dialogs

import com.arellomobile.mvp.MvpView
import com.kms.ustek_msg.domain.repositories.models.TestDialogItem

interface ITestDialogsView : MvpView {

    fun bindDialogs(testDialogs: List<TestDialogItem>)
}