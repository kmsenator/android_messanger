package com.kms.ustek_msg.misc.dialogs

import com.kms.ustek_msg.domain.repositories.models.DialogItem
import com.kms.ustek_msg.domain.repositories.models.TestDialogItem
import com.kms.ustek_msg.domain.repositories.models.UsersItem
import com.kms.ustek_msg.domain.repositories.models.rest.Message
import com.kms.ustek_msg.domain.repositories.models.rest.User

interface ITypeView {
    fun bind(data: TestDialogItem)
    fun bind(data: UsersItem)
    fun bind(data: DialogItem)
    fun bind(data: User)
    fun bind(data: Message)
}