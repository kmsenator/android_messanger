package com.kms.ustek_msg.misc.dialogs

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.kms.ustek_msg.domain.repositories.DialogsRepository
import javax.inject.Inject

@InjectViewState
class TestPresenter : MvpPresenter<ITestDialogsView> {

    private val repository: DialogsRepository

    @Inject
    constructor(repository: DialogsRepository) {
        this.repository = repository
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        repository.loadDialogs {
            viewState.bindDialogs(it)
        }
    }
}