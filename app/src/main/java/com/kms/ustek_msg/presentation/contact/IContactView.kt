package com.kms.ustek_msg.presentation.contact

import com.kms.ustek_msg.base.IBaseView

interface IContactView : IBaseView {
    fun showError(message: String)
}