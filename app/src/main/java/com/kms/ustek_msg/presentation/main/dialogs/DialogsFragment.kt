package com.kms.ustek_msg.presentation.main.dialogs

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.kms.ustek_msg.App
import com.kms.ustek_msg.R
import com.kms.ustek_msg.base.ABaseAdapter
import com.kms.ustek_msg.base.ABaseListFragment
import com.kms.ustek_msg.domain.repositories.models.DialogItem
import com.kms.ustek_msg.misc.dialogs.ITypeView
import com.kms.ustek_msg.misc.dialogs.Type1View
import com.kms.ustek_msg.misc.dialogs.TypeListView
import com.kms.ustek_msg.presentation.main.IMainRouter
import kotlinx.android.synthetic.main.fragment_dialogs_2.*
import javax.inject.Inject

class DialogsFragment : ABaseListFragment<DialogItem, RecyclerView.ViewHolder>(), IDialogsView {

    class Adapter(

        val onSelected: (DialogItem) -> Unit

    ) : ABaseAdapter<DialogItem, RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return object : RecyclerView.ViewHolder(TypeListView(parent.context).apply {
                layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
            }) { }
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            val view = holder.itemView
            if (view is ITypeView) {
                val user = data[position]
                view.bind(user)
                view.setOnClickListener { onSelected(user) }
            }
        }
    }

    @Inject
    @InjectPresenter
    lateinit var presenter: DialogsPresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    private val adapter = Adapter {
        onSelected(it)
    }

    override fun getListId() = R.id.rvList
    override fun getViewId() = R.layout.fragment_dialogs_2
    override fun provideAdapter() = adapter

    override fun inject() {
        App.appComponent.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnAddDialog.setOnClickListener {
            showUsersList()
        }
    }

    override fun setDialogs(dialogs: List<DialogItem>) {
        adapter.data = dialogs.toMutableList()
    }

    private fun showUsersList() {
        activity?.let {
            if (it is IMainRouter)
                it.showUserList()
        }
    }

    private fun onSelected(dialog: DialogItem) {
        activity?.let {
            if (it is IMainRouter)
                it.startDialog(dialog)
        }
    }
}