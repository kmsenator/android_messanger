package com.kms.ustek_msg.presentation.contact

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.kms.ustek_msg.base.SubRX
import com.kms.ustek_msg.domain.repositories.ContactRepository
import com.kms.ustek_msg.domain.repositories.UserRepository
import com.kms.ustek_msg.domain.repositories.models.rest.User
import javax.inject.Inject

@InjectViewState
class ContactPresenter : MvpPresenter<IContactView> {

    private var contactRepository: ContactRepository
    private var userRepository: UserRepository

    @Inject
    constructor(contactRepository: ContactRepository, userRepository: UserRepository) {
        this.contactRepository = contactRepository
        this.userRepository = userRepository
    }


//    fun users(parameter:String) : List<User> {
//
//
//    return    contactRepository.users(SubRX { users, e ->
//
//            users?.let {
//
//                println("users: {$it.id}, {$it.login}")
//                return@let
//            }
//            e?.printStackTrace()
//        },parameter
//        )
//
//    }


}