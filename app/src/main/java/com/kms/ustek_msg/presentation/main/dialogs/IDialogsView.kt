package com.kms.ustek_msg.presentation.main.dialogs

import com.arellomobile.mvp.MvpView
import com.kms.ustek_msg.domain.repositories.models.DialogItem

interface IDialogsView : MvpView {

    fun setDialogs(dialogs: List<DialogItem>)
}