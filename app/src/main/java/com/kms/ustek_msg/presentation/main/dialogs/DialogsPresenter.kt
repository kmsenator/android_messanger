package com.kms.ustek_msg.presentation.main.dialogs

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.kms.ustek_msg.base.SubRX
import com.kms.ustek_msg.domain.repositories.ContactRepository
import javax.inject.Inject

@InjectViewState
class DialogsPresenter @Inject constructor(

    var repository: ContactRepository

) : MvpPresenter<IDialogsView>() {

    override fun attachView(view: IDialogsView?) {
        super.attachView(view)
        loadDialogs()
    }

    fun loadDialogs() {

        repository.loadDialogs(SubRX { dialogs, e ->
            dialogs?.let { viewState.setDialogs(it) }
            e?.printStackTrace()
        })
    }
}