package com.kms.ustek_msg.presentation.main.dialog

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.kms.ustek_msg.base.SubRX
import com.kms.ustek_msg.domain.repositories.ContactRepository
import com.kms.ustek_msg.domain.repositories.MessageRepository
import com.kms.ustek_msg.domain.repositories.models.rest.User
import javax.inject.Inject

@InjectViewState
class DialogPresenter @Inject constructor(

    var repository: MessageRepository

) : MvpPresenter<IDialogView>() {

    fun loadMessages(abonUser: User) {

        repository.getMessages(SubRX { dialogs, e ->
            dialogs?.let { viewState.setMessages(it) }
            e?.printStackTrace()
        }, abonUser.login)
    }

    fun loadNewMessages() {

        repository.getNewMessages(SubRX { dialogs, e ->
            dialogs?.let { viewState.setMessages(it) }
            e?.printStackTrace()
        })
    }

    fun sendMessage(message: String, abonUser: User) {

        viewState.lockMessage(true)
        repository.send(SubRX { msg, e ->
            viewState.lockMessage(false)

            if (msg != null) {
                viewState.onSuccessSend(msg)
                return@SubRX
            }

            e?.let {
                viewState.onError(e.localizedMessage)
            }

        }, abonUser.id, message)
    }
}