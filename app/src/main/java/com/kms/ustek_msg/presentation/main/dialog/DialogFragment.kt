package com.kms.ustek_msg.presentation.main.dialog

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.kms.ustek_msg.App
import com.kms.ustek_msg.R
import com.kms.ustek_msg.base.ABaseAdapter
import com.kms.ustek_msg.base.ABaseListFragment
import com.kms.ustek_msg.domain.repositories.models.DialogItem
import com.kms.ustek_msg.domain.repositories.models.rest.Message
import com.kms.ustek_msg.domain.repositories.models.rest.User
import com.kms.ustek_msg.misc.dialogs.ITypeView
import com.kms.ustek_msg.misc.dialogs.TestFragment
import com.kms.ustek_msg.misc.dialogs.Type1View
import com.kms.ustek_msg.misc.dialogs.Type2View
import kotlinx.android.synthetic.main.fragment_dialog.*
import javax.inject.Inject

class DialogFragment : ABaseListFragment<Message, RecyclerView.ViewHolder>(), IDialogView {

    companion object {

        private const val ARG_DIALOG = "ARG_DIALOG"

        fun create(dialog: DialogItem) = DialogFragment().apply {
            arguments = Bundle().apply {
                putParcelable(ARG_DIALOG, dialog)
            }
        }
    }

    class Adapter(

        val abonUserId: Int

    ) : ABaseAdapter<Message, RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

            val view: View = if (viewType == TestFragment.Adapter.TYPE_1) Type1View(parent.context)
            else Type2View(parent.context)
            view.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            return object : RecyclerView.ViewHolder(view) { }
        }

        override fun getItemViewType(position: Int): Int {
            return if (data[position].from == abonUserId) TestFragment.Adapter.TYPE_1 else TestFragment.Adapter.TYPE_2
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            val view = holder.itemView
            if (view is ITypeView)
                view.bind(data[position])
        }
    }

    @Inject
    @InjectPresenter
    lateinit var presenter: DialogPresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    private lateinit var adapter: Adapter
    private lateinit var abonUser: User

    override fun getListId() = R.id.rvList
    override fun getViewId() = R.layout.fragment_dialog

    override fun provideLayoutManager(): RecyclerView.LayoutManager {
        return LinearLayoutManager(context).apply { reverseLayout = true }
    }

    override fun provideAdapter() = adapter

    override fun inject() {
        App.appComponent.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val dialog = getArgDialog()
        abonUser = dialog.abonUser ?: throw IllegalArgumentException("User undefined")
        adapter = Adapter(abonUser.id)

        super.onViewCreated(view, savedInstanceState)

        etMessage.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) { validateMessage() }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }
        })

        btnSend.setOnClickListener {
            presenter.sendMessage("${etMessage.text}", abonUser)
        }

        validateMessage()
    }

    override fun onStart() {
        super.onStart()
        presenter.loadMessages(abonUser)
    }

    fun loadNewMessages() {
        presenter.loadNewMessages()
    }

    private fun getArgDialog() =
        arguments?.getParcelable<DialogItem>(ARG_DIALOG) ?: throw IllegalArgumentException("Dialog undefined")

    private fun validateMessage() {
        btnSend.isEnabled = etMessage.text.isNotEmpty()
    }


    override fun setMessages(messages: List<Message>) {
        adapter.data = messages.toMutableList()
    }

    override fun onSuccessSend(message: Message) {
        adapter.addBegin(message)
        etMessage.text = null
        rvList.scrollToPosition(0)
    }

    override fun lockMessage(value: Boolean) {
        llMessage.isEnabled = value
    }
}