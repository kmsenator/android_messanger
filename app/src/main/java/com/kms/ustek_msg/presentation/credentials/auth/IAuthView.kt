package com.kms.ustek_msg.presentation.credentials.auth

import com.kms.ustek_msg.base.IBaseView

interface IAuthView : IBaseView {
    fun onSuccess()
}