package com.kms.ustek_msg.presentation.credentials

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.ActionBar
import com.kms.ustek_msg.App
import com.kms.ustek_msg.R
import com.kms.ustek_msg.base.ABaseActivity
import com.kms.ustek_msg.domain.repositories.local.UserStorage
import com.kms.ustek_msg.presentation.credentials.auth.AuthFragment
import com.kms.ustek_msg.presentation.credentials.loading.LoadingFragment
import com.kms.ustek_msg.presentation.credentials.registration.RegistrationFragment


class CredentialsActivity : ABaseActivity(), ICredentialsRouter {

    companion object {

        private const val ARG_DROP_CREDENTIALS = "ARG_DROP_CREDENTIALS"

        fun show() {
            App.appContext.let {
                it.startActivity(Intent(it, CredentialsActivity::class.java).apply {
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    putExtra(ARG_DROP_CREDENTIALS, true)
                })
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_container)

        if (savedInstanceState != null)
            return

        // Немного не верно с т.з. архитектуры, но тут больше и не нужно
        if (intent.getBooleanExtra(ARG_DROP_CREDENTIALS, false)) {
            UserStorage().dropCredentials()
            println("lya")
            showAuth()
            return
        }

        showLoading()
    }

    override fun showLoading() {
        replace(LoadingFragment())
    }

    override fun showRegistration() {
        replace(RegistrationFragment(), "Registration")
    }

    override fun showAuth() {
        replace(AuthFragment())
    }
}