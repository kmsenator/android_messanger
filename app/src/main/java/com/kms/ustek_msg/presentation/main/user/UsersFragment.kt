package com.kms.ustek_msg.presentation.main.user

import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bumptech.glide.Glide
import com.kms.ustek_msg.App
import com.kms.ustek_msg.R
import com.kms.ustek_msg.base.ABaseAdapter
import com.kms.ustek_msg.base.ABaseListFragment
import com.kms.ustek_msg.domain.repositories.models.rest.User
import com.kms.ustek_msg.misc.dialogs.ITypeView
import com.kms.ustek_msg.misc.dialogs.TypeListView
import com.kms.ustek_msg.presentation.main.IMainRouter
import kotlinx.android.synthetic.main.item_contact.view.*
import kotlinx.android.synthetic.main.view_dialog_type_list.*
import javax.inject.Inject

class UsersFragment : ABaseListFragment<User, RecyclerView.ViewHolder>(), IUsersView {

    class Adapter(

        val onSelected: (User) -> Unit

    ) : ABaseAdapter<User, RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return object : RecyclerView.ViewHolder(TypeListView(parent.context).apply {
                layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
//                Glide
//                    .with(this)
//                    .load(User.avatarUrl)
//                    .into(findViewById(R.id.ivAvatar))
            }) { }
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            val view = holder.itemView
            if (view is ITypeView) {
                val user = data[position]

                view.bind(user)
                view.setOnClickListener { onSelected(user) }
            }
        }
    }

    @Inject
    @InjectPresenter
    lateinit var presenter: UsersPresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    private val adapter = Adapter {
        presenter.createDialog(it)
        activity?.onBackPressed()
    }

    override fun getListId() = R.id.rvList
    override fun getViewId() = R.layout.fragment_users
    override fun provideAdapter() = adapter

    override fun inject() {
        App.appComponent.inject(this)
    }

    override fun setUsers(users: List<User>) {
        adapter.data = users.toMutableList()
    }

    private fun showUsersList() {
        activity?.let {
            if (it is IMainRouter)
                it.showDialogList()
        }
    }
}