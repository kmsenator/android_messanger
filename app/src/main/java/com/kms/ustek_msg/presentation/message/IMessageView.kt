package com.kms.ustek_msg.presentation.message

import com.kms.ustek_msg.base.IBaseView

interface IMessageView : IBaseView {
    fun showError(message: String)
}