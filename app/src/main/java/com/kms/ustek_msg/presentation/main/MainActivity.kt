package com.kms.ustek_msg.presentation.main

import android.content.Intent
import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.kms.ustek_msg.App
import com.kms.ustek_msg.R
import com.kms.ustek_msg.base.ABaseActivity
import com.kms.ustek_msg.domain.repositories.models.DialogItem
import com.kms.ustek_msg.presentation.contact.ContactActivity
import com.kms.ustek_msg.presentation.credentials.CredentialsActivity
import com.kms.ustek_msg.presentation.main.dialog.DialogFragment
import com.kms.ustek_msg.presentation.main.dialog.DialogPresenter
import com.kms.ustek_msg.presentation.main.dialogs.DialogsFragment
import com.kms.ustek_msg.presentation.main.user.UsersFragment
import com.kms.ustek_msg.service.AlarmService
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : ABaseActivity(), IMainRouter {

    companion object {

        fun show() {
           // EnigmaActivity.show()
            App.appContext.let {
                it.startActivity(Intent(it, MainActivity::class.java).apply {
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                })
            }
        }
    }

//    @Inject
//    @InjectPresenter
//    lateinit var presenter: EnigmaPresenter
//
//    @ProvidePresenter
//    fun providePresenter() = presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState != null)
            return

        showDialogList()
//               supportFragmentManager.beginTransaction().replace(
//                    R.id.container, RegistrationFragment()
//                ).commit()
     //   replace(DialogsFragment())

//        btnLogout.setOnClickListener {
//            CredentialsActivity.show()
//        }
//
//        btnContact.setOnClickListener {
//            ContactActivity.show()
//        }
        AlarmService.exec(Bundle().apply {
            putLong("TIME", System.currentTimeMillis())
            //presenter.loadNewMessages()
        }, 2000, 1000)
    }

    override fun showDialogList() {
        replace(DialogsFragment())
    }

    override fun showUserList() {
        replace(UsersFragment(), "users-list")
    }

    override fun startDialog(dialog: DialogItem) {
        replace(DialogFragment.create(dialog), "dialog")


    }
}
