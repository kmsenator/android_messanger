package com.kms.ustek_msg.presentation.main.dialog

import com.arellomobile.mvp.MvpView
import com.kms.ustek_msg.domain.repositories.models.DialogItem
import com.kms.ustek_msg.domain.repositories.models.rest.Message

interface IDialogView : MvpView {

    fun setMessages(messages: List<Message>)
    fun onSuccessSend(message: Message)
    fun lockMessage(value: Boolean)
    fun onError(message: String)
}