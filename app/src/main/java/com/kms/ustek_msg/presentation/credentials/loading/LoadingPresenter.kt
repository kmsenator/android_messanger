package  com.kms.ustek_msg.presentation.credentials.loading

import android.os.Handler
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.kms.ustek_msg.domain.repositories.UserRepository
import com.kms.ustek_msg.domain.repositories.models.rest.User
import com.kms.ustek_msg.presentation.main.MainActivity
import javax.inject.Inject

@InjectViewState
class LoadingPresenter : MvpPresenter<ILoadingView> {

    private val userRepository : UserRepository
    @Inject
    constructor(userRepository: UserRepository) {
        this.userRepository = userRepository
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        loadStaticResources()
    }

    fun loadStaticResources() {
        Handler().postDelayed({

            val  user : User? = userRepository.getUser()
            if (user != null) {
                MainActivity.show()
                return@postDelayed
            }
            viewState.showAuth()

        }, 2000)
    }
}