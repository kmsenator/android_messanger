package com.kms.ustek_msg.presentation.credentials.auth

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.kms.ustek_msg.base.SubRX
import com.kms.ustek_msg.domain.repositories.UserRepository
import com.kms.ustek_msg.presentation.main.MainActivity
import javax.inject.Inject


@InjectViewState
class AuthPresenter : MvpPresenter<IAuthView> {

    @Inject
    lateinit var userRepository: UserRepository

    @Inject
    constructor()

    fun auth(login: String, password: String) {

        userRepository.login(SubRX { _, e ->

            if (e != null) {
                e.printStackTrace()
                viewState.onError(e.localizedMessage)
                return@SubRX
            }

            MainActivity.show()
        }, login, password)
    }
}