package com.kms.ustek_msg.presentation.credentials

interface ICredentialsRouter {
    fun showLoading()
    fun showRegistration()
    fun showAuth()
}