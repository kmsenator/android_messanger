package com.kms.ustek_msg.presentation.credentials.auth

import android.os.Bundle
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.kms.ustek_msg.R
import com.kms.ustek_msg.base.ABaseFragment
import com.kms.ustek_msg.domain.di.components.DaggerAppComponent
import com.kms.ustek_msg.presentation.credentials.ICredentialsRouter
import kotlinx.android.synthetic.main.fragment_auth.*
import kotlinx.android.synthetic.main.item_message.*
import javax.inject.Inject

class AuthFragment : ABaseFragment(), IAuthView {

    @Inject
    @InjectPresenter
    lateinit var presenter: AuthPresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    override fun inject() {
        DaggerAppComponent.create().inject(this)
    }

    override fun getViewId() = R.layout.fragment_auth

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnLogin.setOnClickListener {

            val login = "${etLogin.text}"
            val password = "${etPassword.text}"
            val errorMessageCredentialsEmpty = getString(R.string.credentialsEmpty)
            if (login.isEmpty() || password.isEmpty()) {
                toast("Error: $errorMessageCredentialsEmpty")
                return@setOnClickListener
            }

            presenter.auth(login, password)
        }

        btnRegistration.setOnClickListener {
            activity?.let {
                if (it is ICredentialsRouter)
                    it.showRegistration()
            }
        }

    }

    override fun lock() {
        visibility(flBtnContainer)
    }

    override fun unlock() {
        visibility(flBtnContainer, false)
    }

    override fun onSuccess() {

        toast("SUCCESS")
        // Отправить на главную форму
    }
}