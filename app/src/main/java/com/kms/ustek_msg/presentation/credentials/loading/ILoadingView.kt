package com.kms.ustek_msg.presentation.credentials.loading

import com.arellomobile.mvp.MvpView

interface ILoadingView : MvpView {
    fun showAuth()
}