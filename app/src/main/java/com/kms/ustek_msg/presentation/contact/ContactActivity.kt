package com.kms.ustek_msg.presentation.contact

import android.content.Intent
import android.os.Bundle
import com.kms.ustek_msg.App
import com.kms.ustek_msg.R
import com.kms.ustek_msg.base.ABaseActivity

class ContactActivity : ABaseActivity() {

    companion object {

        fun show() {
            App.appContext.let {
                it.startActivity(Intent(it, ContactActivity::class.java).apply {
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                })
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_container)

        if (savedInstanceState != null)
            return

        replace(ContactFragment())
    }
}