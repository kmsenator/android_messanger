package com.kms.ustek_msg.presentation.main

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.kms.ustek_msg.base.SubRX
import com.kms.ustek_msg.domain.repositories.ContactRepository
import com.kms.ustek_msg.domain.repositories.DialogsRepository
import com.kms.ustek_msg.domain.repositories.UserRepository
import com.kms.ustek_msg.presentation.credentials.CredentialsActivity
import java.io.File
import javax.inject.Inject

@InjectViewState
class EnigmaPresenter : MvpPresenter<IEnigmaView> {

    private var userRepository: UserRepository
    private var contactRepository: ContactRepository

    @Inject
    constructor(
        userRepository: UserRepository,
        contactRepository: ContactRepository

    ) {
        this.userRepository = userRepository
        this.contactRepository = contactRepository

//        contactRepository.users(SubRX { users, e ->
//                users?.let { println("users: $users") }
//                e?.printStackTrace()
//            },"")

    }


    fun upload(file: File) {
        println("Upload file: $file")

        viewState.lock()
        contactRepository.uploadAvatar(SubRX { path, e ->

            viewState.unlock()
            e?.let {
                viewState.onError(it.localizedMessage)
                it.printStackTrace()
                return@SubRX
            }

            path?.let {
                println("Link: $it")
                viewState.onSuccess(it.path)
            }

        }, file)
    }

    fun logout() {
        userRepository.logout()
        CredentialsActivity.show()
    }
}