package com.kms.ustek_msg.presentation.main.user

import android.os.Handler
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.kms.ustek_msg.base.SubRX
import com.kms.ustek_msg.domain.repositories.ContactRepository
import com.kms.ustek_msg.domain.repositories.models.rest.User
import javax.inject.Inject

@InjectViewState
class UsersPresenter @Inject constructor(

    var repository: ContactRepository

) : MvpPresenter<IUsersView>() {

    private var isAttache = false
    private var handler = Handler()

    override fun attachView(view: IUsersView?) {
        isAttache = true
        super.attachView(view)
        loadUsers()
    }

    override fun detachView(view: IUsersView?) {
        isAttache = false
        super.detachView(view)
    }

    fun loadUsers() {
        repository.getUsers(SubRX { users, e ->
            if (users != null) {
                viewState.setUsers(users)
                return@SubRX
            }

            e?.printStackTrace()
            handler.postDelayed({
                if (isAttache)
                    loadUsers()
            }, 500)
        })
    }

    fun createDialog(user: User) {
        repository.createDialog(user)
    }
}