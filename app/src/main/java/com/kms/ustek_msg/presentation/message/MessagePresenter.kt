package com.kms.ustek_msg.presentation.message

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.kms.ustek_msg.domain.repositories.MessageRepository
import javax.inject.Inject

@InjectViewState
class MessagePresenter : MvpPresenter<IMessageView> {


    @Inject
    lateinit var messageRepository: MessageRepository

    @Inject
    constructor(messageRepository: MessageRepository) {
        this.messageRepository = messageRepository
    }

    init {

    }
    //
}