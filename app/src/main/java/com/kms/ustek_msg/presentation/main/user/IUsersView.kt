package com.kms.ustek_msg.presentation.main.user

import com.arellomobile.mvp.MvpView
import com.kms.ustek_msg.domain.repositories.models.DialogItem
import com.kms.ustek_msg.domain.repositories.models.rest.User

interface IUsersView : MvpView {

    fun setUsers(users: List<User>)
}