package com.kms.ustek_msg.presentation.main

import com.kms.ustek_msg.domain.repositories.models.DialogItem

interface IMainRouter {

    fun showDialogList()
    fun showUserList()
    fun startDialog(dialog: DialogItem)
}