package com.kms.ustek_msg.presentation.credentials.registration

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.kms.ustek_msg.base.SubRX
import com.kms.ustek_msg.presentation.main.MainActivity
import com.kms.ustek_msg.domain.repositories.UserRepository
import javax.inject.Inject

@InjectViewState
class RegistrationPresenter : MvpPresenter<IRegistrationView> {

    @Inject
    lateinit var userRepository: UserRepository

    @Inject
    constructor()

    fun registration(login: String, pass: String) {

        viewState.lock()
        userRepository.registration(SubRX { _, e ->
            viewState.unlock()

            if (e != null) {
                e.printStackTrace()
                val errorRegistration400 = "The login already exists!"
                if (e.localizedMessage.trim() == "HTTP 400") {
                    println("*"+e.localizedMessage+"*")
                    viewState.onError("Error: $errorRegistration400")
                } else {
                    println("="+e.localizedMessage+"=")
                    viewState.onError(e.localizedMessage)
                }
                return@SubRX
            }

            MainActivity.show()

        }, login, pass)
    }
}