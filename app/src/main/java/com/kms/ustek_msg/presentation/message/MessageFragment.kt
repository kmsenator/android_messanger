package com.kms.ustek_msg.presentation.message

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.kms.ustek_msg.R
import com.kms.ustek_msg.base.ABaseFragment
import com.kms.ustek_msg.domain.di.components.DaggerAppComponent
import com.kms.ustek_msg.presentation.contact.ContactPresenter
import com.kms.ustek_msg.presentation.message.IMessageView
import com.kms.ustek_msg.presentation.message.MessagePresenter
import javax.inject.Inject

class MessageFragment : ABaseFragment(), IMessageView {
    @Inject
    @InjectPresenter
    lateinit var presenter: MessagePresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    override fun inject() {
        DaggerAppComponent.create().inject(this)
    }

    override fun getViewId() = R.layout.fragment_contact

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //

    }

    override fun showError(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }
}