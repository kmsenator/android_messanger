package com.kms.ustek_msg.presentation.contact

import com.arellomobile.mvp.MvpView

interface IProfileView : MvpView {
    fun showError(message: String)
}