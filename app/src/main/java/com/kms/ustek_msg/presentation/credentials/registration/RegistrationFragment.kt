package com.kms.ustek_msg.presentation.credentials.registration

import android.os.Bundle
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.kms.ustek_msg.R
import com.kms.ustek_msg.base.ABaseFragment
import com.kms.ustek_msg.domain.di.components.DaggerAppComponent
import com.kms.ustek_msg.presentation.credentials.ICredentialsRouter
import kotlinx.android.synthetic.main.fragment_registration.*
import kotlinx.android.synthetic.main.fragment_registration.btnLogin
import kotlinx.android.synthetic.main.fragment_registration.btnRegistration
import kotlinx.android.synthetic.main.fragment_registration.etLogin
import kotlinx.android.synthetic.main.fragment_registration.etPassword
import javax.inject.Inject


class RegistrationFragment : ABaseFragment(), IRegistrationView {

    @Inject
    @InjectPresenter
    lateinit var presenter: RegistrationPresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    override fun inject() {
        DaggerAppComponent.create().inject(this)
    }

    override fun getViewId() = R.layout.fragment_registration

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnRegistration.setOnClickListener {
            val login = "${etLogin.text}"
            val password = "${etPassword.text}"
            val confirmPassword = "${etConfirmPassword.text}"
            val errorMessageLoginEmpty = getString(R.string.loginEmpty)
            val errorMessagePasswordEmpty = getString(R.string.passwordEmpty)
            val errorMessageConfirmPassword = getString(R.string.noMatchConfirmationPassword)

            if (login.isEmpty()) {
                toast("Error: $errorMessageLoginEmpty")
                return@setOnClickListener
            } else if (password.isEmpty()) {
                toast("Error: $errorMessagePasswordEmpty")
                return@setOnClickListener
            } else if (password != confirmPassword) {
                toast("Error: $errorMessageConfirmPassword")
                return@setOnClickListener
            } else {
                presenter.registration(login, password)
            }

        }

        btnLogin.setOnClickListener {
            activity?.let {
                if (it is ICredentialsRouter)
                    it.showAuth()
            }
        }
    }
}