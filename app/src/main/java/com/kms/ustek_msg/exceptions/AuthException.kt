package com.kms.ustek_msg.exceptions

class AuthException(message: String) : Exception(message)