package com.kms.ustek_msg.domain.repositories.rest.api

import com.kms.ustek_msg.base.ABaseRestApi
import com.kms.ustek_msg.base.IRestClient
import com.kms.ustek_msg.domain.repositories.models.rest.User
import com.kms.ustek_msg.domain.repositories.models.rest.UserUpdate
import com.kms.ustek_msg.domain.repositories.rest.service.IContactRestApiService
import com.soft.eac.thedepartmentgl.domain.repositories.models.rest.UploadedFile
import io.reactivex.Observable
import okhttp3.MediaType
import okhttp3.MultipartBody
import java.io.File
import javax.inject.Inject

class ContactRestApi: ABaseRestApi<IContactRestApiService> {

    @Inject
    constructor(client: IRestClient) : super(client)

    /**
     * Get User List
     */
    fun getUsers(included : String? = null) : Observable<List<User>> {
        return service.getUsers(included)
    }

    /**
     * Load avatar profile. Max size 256 KB.
     */
    fun uploadAvatar(file: File): Observable<UploadedFile> {

        val part = MultipartBody.Part.createFormData("file",
            file.name + ".jpg",
            MultipartBody.create(MediaType.parse("image/*"), file)
        )

        return service.uploadAvatar(part)
    }

    /**
     * Update user profile
     */
    fun updateUser(userUpdate: UserUpdate) = service.userUpdate(userUpdate)
}