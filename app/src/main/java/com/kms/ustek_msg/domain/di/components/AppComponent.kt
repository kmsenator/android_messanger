package com.kms.ustek_msg.domain.di.components

import com.kms.ustek_msg.domain.di.modules.NetModule
import com.kms.ustek_msg.misc.dialogs.TestFragment
import com.kms.ustek_msg.presentation.contact.ContactFragment
import com.kms.ustek_msg.presentation.credentials.auth.AuthFragment
import com.kms.ustek_msg.presentation.credentials.loading.LoadingFragment
import com.kms.ustek_msg.presentation.credentials.registration.RegistrationFragment
import com.kms.ustek_msg.presentation.main.EnigmaActivity
import com.kms.ustek_msg.presentation.main.dialog.DialogFragment
import com.kms.ustek_msg.presentation.main.dialogs.DialogsFragment
import com.kms.ustek_msg.presentation.main.user.UsersFragment
import com.kms.ustek_msg.presentation.message.MessageFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    NetModule::class
])
interface AppComponent {

    fun inject(target: RegistrationFragment)
    fun inject(target: AuthFragment)
    fun inject(target: LoadingFragment)
    fun inject(target: TestFragment)
    fun inject(target: ContactFragment)
    fun inject(target: MessageFragment)
    fun inject(target: EnigmaActivity)

    fun inject(target: DialogsFragment)
    fun inject(target: UsersFragment)
    fun inject(target: DialogFragment)
}