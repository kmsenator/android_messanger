package com.kms.ustek_msg.domain.repositories.local

import com.kms.ustek_msg.domain.repositories.models.DialogItem
import com.kms.ustek_msg.domain.repositories.models.realm.DialogItemRealm
import com.kms.ustek_msg.domain.repositories.models.rest.User
import com.kms.ustek_msg.domain.repositories.models.toBase
import com.kms.ustek_msg.domain.repositories.models.toRealm
import io.realm.Realm
import javax.inject.Inject

class MainStorage @Inject constructor() {


    fun getDialogs(): List<DialogItem> {
        return Realm.getDefaultInstance().use {
            it.where(DialogItemRealm::class.java).findAll().toBase() ?: listOf()
        }
    }

    fun hasDialog(user: User): Boolean {
        return Realm.getDefaultInstance().use {
            it.where(DialogItemRealm::class.java).equalTo("abonUser.login", user.login).findFirst() != null
        }
    }

    fun addDialog(dialog: DialogItem) {
        Realm.getDefaultInstance().use {
            it.executeTransaction {
                it.copyToRealmOrUpdate(dialog.toRealm())
            }
        }
    }
}