package com.kms.ustek_msg.domain.repositories.rest.service

import com.kms.ustek_msg.domain.repositories.models.rest.Message
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface IMessageRestApiService {

    /**
     * Получить всю историю сообщений (от новых к старым)
     */
    @GET("/messenger/v1/messages")
    fun getMessages(
        @Query("from") from: String, // от кого
        @Query("limit") limit: Int? = null,  // количество
        @Query("page") page: Int? = null //пагинация
    ): Observable<List<Message>>

    /**
     * Получить сообщения (только новые)
     */
    @GET("/messenger/v1/new_messages")
    fun getNewMessages(): Observable<List<Message>>

    /**
     * Отправка сообщения пользователю
     */
    @POST("/messenger/v1/send")
    fun send(@Body message: Message): Observable<Message>

}