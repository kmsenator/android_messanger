package com.kms.ustek_msg.domain.repositories

import com.kms.ustek_msg.domain.repositories.models.TestDialogItem
import java.util.*
import javax.inject.Inject

class DialogsRepository {

    @Inject
    constructor()

    fun loadDialogs(call: (List<TestDialogItem>) -> Unit) {

        val result = mutableListOf<TestDialogItem>()
        val random = Random(System.currentTimeMillis())
        val count = random.nextInt(900) + 100
        for (index in 0 until count)
            result.add(TestDialogItem("Title: $index", random.nextBoolean()))

        call(result)
    }
}