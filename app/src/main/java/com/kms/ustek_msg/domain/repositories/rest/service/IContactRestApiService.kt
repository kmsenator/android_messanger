package com.kms.ustek_msg.domain.repositories.rest.service

import com.kms.ustek_msg.domain.repositories.models.rest.User
import com.kms.ustek_msg.domain.repositories.models.rest.UserUpdate
import com.soft.eac.thedepartmentgl.domain.repositories.models.rest.UploadedFile
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.http.*

interface IContactRestApiService {

    /**
     * Get Users list
     */
    @GET("/user/v1/users")
    fun getUsers(
        @Query("included") included : String? = null
    ): Observable<List<User>>

    /**
     * Load profile avatar. Max size 256 KB.
     */
    @Multipart
    @POST("/upload/v1/avatar")
    fun uploadAvatar(@Part file: MultipartBody.Part): Observable<UploadedFile>

    /**
     *  Update User profile
     */
    @POST("/upload/v1/update")
    fun userUpdate(@Body userUpdate: UserUpdate): Observable<UserUpdate>
}