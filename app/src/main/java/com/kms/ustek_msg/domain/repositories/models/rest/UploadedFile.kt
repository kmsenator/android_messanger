package com.soft.eac.thedepartmentgl.domain.repositories.models.rest

/**
 * Data class. Позволяет загружать файл для аватаров пользователей. Вес изображения не более 256 кб.
 */
data class UploadedFile(
    val path: String
)