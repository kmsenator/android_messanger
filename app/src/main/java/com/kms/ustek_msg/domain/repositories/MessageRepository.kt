package com.kms.ustek_msg.domain.repositories

import com.kms.ustek_msg.base.SubRX
import com.kms.ustek_msg.base.standardSubscribeIO
import com.kms.ustek_msg.domain.repositories.local.UserStorage
import com.kms.ustek_msg.domain.repositories.models.rest.Message
import com.kms.ustek_msg.domain.repositories.rest.api.MessageRestApi
import javax.inject.Inject

class MessageRepository {

    private val storage: UserStorage
    private val rest: MessageRestApi

    @Inject
    constructor(storage: UserStorage, rest: MessageRestApi) {
        this.storage = storage
        this.rest = rest
    }

    /**
     * @param observer
     */
    fun getMessages(observer: SubRX<List<Message>>, userIdOrLogin: String) {

        rest.getMessages(userIdOrLogin)
            .standardSubscribeIO(observer)
    }

    /**
     * @param observer
     */
    fun getNewMessages(observer: SubRX<List<Message>>) {

        rest.getNewMessages()
            .standardSubscribeIO(observer)
    }

    /**
     * @param observer
     */
    fun send(observer: SubRX<Message>, to :Int, message: String) {

//        val currentUser = storage.getUser()
//        if (currentUser == null) {
//            observer.onError(IllegalStateException("Current user undefined"))
//            return
//        }

        rest.send(Message(to, message))
            .standardSubscribeIO(observer)
    }

}