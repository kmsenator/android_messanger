package com.kms.ustek_msg.domain.repositories

import android.os.SystemClock
import com.kms.ustek_msg.base.SubRX
import com.kms.ustek_msg.base.standardSubscribeIO
import com.kms.ustek_msg.domain.repositories.models.rest.Token
import com.kms.ustek_msg.domain.repositories.models.rest.User
import com.kms.ustek_msg.domain.repositories.local.UserStorage
import com.kms.ustek_msg.domain.repositories.rest.api.UserRestApi
import java.net.HttpURLConnection
import javax.inject.Inject
import kotlin.String as String

class UserRepository @Inject constructor(
    private val storage: UserStorage,
    private val rest: UserRestApi
) {

    /**
     * User's Registration
     */
    fun registration(observer: SubRX<User>, login: String, pass: String) {

        rest.registration(login, pass)
            .doOnNext { storage.save(it) }
            .standardSubscribeIO(observer)
    }

    /**
     * User's Authentication
     */
    fun login(observer: SubRX<User>, login: String, pass: String) {

        rest.login(login, pass)
            .doOnNext { storage.save(it) }
            .standardSubscribeIO(observer)
    }

    /**
     * Logout from empty storage realm user
     */
    fun logout() {
        storage.dropCredentials()
    }

    /**
     * just get user from storage realm
     */
    fun getUser() = storage.getUser()

    /**
     * Update and refresh token. Method get refresh token when main is not actual.
     */
    fun getToken() = storage.getToken()

    fun refreshToken(token: Token, onRetry: (Int) -> Boolean = { it != HttpURLConnection.HTTP_UNAUTHORIZED }): Token? {

        val response = rest.refreshToken(token.refresh).execute()
        if (response.isSuccessful)
            response.body()?.let {
                it.refresh = token.refresh
                storage.save(it)
                return it
            }

        if (onRetry(response.code())) {
            SystemClock.sleep(500)
            return refreshToken(token)
        }

        return null
    }

}