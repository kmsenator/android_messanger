package com.kms.ustek_msg.domain.repositories.models

data class TestDialogItem(
    val title: String,
    val isType1: Boolean
)