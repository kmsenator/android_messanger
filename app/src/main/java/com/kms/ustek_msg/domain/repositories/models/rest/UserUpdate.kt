package com.kms.ustek_msg.domain.repositories.models.rest

import com.google.gson.annotations.SerializedName

/**
 * Data class. Обновление данных пользователя. Установка аватара и смена пароля
 */
data class UserUpdate (
    @SerializedName("avatar_success")
    val avatarSuccess: Boolean? = false,
    @SerializedName("new_avatar_url")
    val newAvatarUrl: String? = null,
    @SerializedName("new_password")
    val newPassword:	String? = null,
    @SerializedName("old_password")
    val oldPassword:	String? = null,
    @SerializedName("password_success")
    val passwordSuccess:	Boolean? = false
)
