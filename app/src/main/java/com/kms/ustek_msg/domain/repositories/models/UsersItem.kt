package com.kms.ustek_msg.domain.repositories.models

data class UsersItem(
    val title: String,
    val avatarUrl: String,
    val isType: Boolean
)