package com.kms.ustek_msg.domain.repositories.models

import com.kms.ustek_msg.domain.repositories.models.realm.DialogItemRealm
import com.kms.ustek_msg.domain.repositories.models.realm.TokenRealm
import com.kms.ustek_msg.domain.repositories.models.realm.UserRealm
import com.kms.ustek_msg.domain.repositories.models.rest.Token
import com.kms.ustek_msg.domain.repositories.models.rest.User

fun Token?.toRealm() : TokenRealm? {
    this ?: return null
    return TokenRealm().let {
        it.access = access
        it.refresh = refresh
        it
    }
}

fun TokenRealm?.toBase() : Token? {
    this ?: return null
    return Token(access?:"", refresh?:"")
}

fun User?.toRealm() : UserRealm? {
    this ?: return null
    return UserRealm().let {
        it.id = id
        it.login = login
        it.password = password
        it.avatarUrl= avatarUrl
        it.token= token.toRealm()
        it
    }
}

fun UserRealm?.toBase() : User? {
    this ?: return null
    return User(login?:"",password?:"", avatarUrl?:"", token.toBase(), id)
}

fun DialogItem?.toRealm() : DialogItemRealm? {
    this ?: return null
    return DialogItemRealm().let {
        it.createAt = createAt
        it.abonUser= abonUser.toRealm()
        it
    }
}

fun DialogItemRealm?.toBase() : DialogItem? {
    this ?: return null
    return DialogItem(abonUser.toBase(), createAt)
}


fun Collection<DialogItemRealm>?.toBase() : List<DialogItem>? {
    this ?: return null
    val result = mutableListOf<DialogItem>()
    this.forEach { it.toBase()?.let { result.add(it) } }
    return result
}