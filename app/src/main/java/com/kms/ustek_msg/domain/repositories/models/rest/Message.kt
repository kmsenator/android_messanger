package com.kms.ustek_msg.domain.repositories.models.rest

/**
 * Модель сообщения пользователей
 */
data class Message (

    val to: Int,

    val message: String?,

    val from:Int = 0,

    val date: String? = null,

    val delivered: Boolean = false,

    val id: Int = 0
)