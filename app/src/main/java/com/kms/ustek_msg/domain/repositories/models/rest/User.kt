package com.kms.ustek_msg.domain.repositories.models.rest

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.kms.ustek_msg.domain.repositories.models.rest.Token
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    val login: String,
    val password: String? = null,
    @SerializedName("avatar_url")
    val avatarUrl: String? = null,
    var token: Token? = null,
    val id: Int = 0
) : Parcelable