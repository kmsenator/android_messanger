package com.kms.ustek_msg.domain.repositories.models.rest

/**
 * Data class. Получить список всех пользователей
 */
data class UserEntity(
    val id: Int? = null,
    val login: String
)