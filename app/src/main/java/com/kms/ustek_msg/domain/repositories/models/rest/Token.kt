package com.kms.ustek_msg.domain.repositories.models.rest

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Token(
    val access: String,
    var refresh: String
) : Parcelable