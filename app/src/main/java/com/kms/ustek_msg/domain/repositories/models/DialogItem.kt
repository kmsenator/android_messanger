package com.kms.ustek_msg.domain.repositories.models

import android.os.Parcelable
import com.kms.ustek_msg.domain.repositories.models.rest.User
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DialogItem(
    val abonUser: User?,
    val createAt: Long = System.currentTimeMillis()
): Parcelable