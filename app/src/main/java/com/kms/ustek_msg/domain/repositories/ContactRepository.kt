package com.kms.ustek_msg.domain.repositories

import com.kms.ustek_msg.base.standardSubscribeIO
import com.kms.ustek_msg.domain.repositories.local.MainStorage
import com.kms.ustek_msg.domain.repositories.models.DialogItem
import com.kms.ustek_msg.domain.repositories.models.rest.User
import com.kms.ustek_msg.domain.repositories.rest.api.ContactRestApi
import com.soft.eac.thedepartmentgl.domain.repositories.models.rest.UploadedFile
import java.io.File
import javax.inject.Inject
import com.kms.ustek_msg.base.SubRX as SubRX

class ContactRepository {

    private val rest: ContactRestApi
    private val storage: MainStorage

    @Inject
    constructor(rest: ContactRestApi, storage: MainStorage) {
        this.rest = rest
        this.storage = storage
    }

    /**
     * Получить список всех пользователей
     */

    fun getUsers(observer: SubRX<List<User>>, included: String? = null) {
        rest.getUsers(included)
            .standardSubscribeIO(observer)
    }

    /**
     * Upload Profile Avatar, max size 256 KB.
     */
    fun uploadAvatar(observer: SubRX<UploadedFile>, file: File) {

        rest.uploadAvatar(file)
            .standardSubscribeIO(observer)
    }

    /**
     * Update profile
     */


    // ======


    fun loadDialogs(observer: SubRX<List<DialogItem>>) {
        observer.onComplete(storage.getDialogs())
    }

    fun createDialog(user: User) {

        if (!storage.hasDialog(user))
            storage.addDialog(DialogItem(user))
    }
}
