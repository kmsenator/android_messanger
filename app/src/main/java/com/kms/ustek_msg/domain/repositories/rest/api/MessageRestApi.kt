package com.kms.ustek_msg.domain.repositories.rest.api

import com.kms.ustek_msg.base.ABaseRestApi
import com.kms.ustek_msg.base.IRestClient
import com.kms.ustek_msg.domain.di.modules.NetModule
import com.kms.ustek_msg.domain.repositories.models.rest.Message
import com.kms.ustek_msg.domain.repositories.rest.service.IMessageRestApiService
import javax.inject.Inject
import javax.inject.Named

class MessageRestApi : ABaseRestApi<IMessageRestApiService> {

    @Inject
    constructor(client: IRestClient) : super(client)

    fun getMessages(from: String) = service.getMessages(from)

    fun getNewMessages() = service.getNewMessages()

    fun send(message: Message) = service.send(message)

}