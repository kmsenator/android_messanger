package com.kms.ustek_msg.domain.repositories.models.realm

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class DialogItemRealm : RealmObject () {

    @PrimaryKey
    var createAt: Long = 0L
    var abonUser: UserRealm? = null
}