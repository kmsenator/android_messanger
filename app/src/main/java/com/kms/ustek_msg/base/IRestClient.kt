package com.kms.ustek_msg.base

interface IRestClient {
    fun <S> createService(serviceClass: Class<S>): S
    fun cancelAllRequests()
}